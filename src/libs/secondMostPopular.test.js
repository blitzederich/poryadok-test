// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

const { secondMostPopularChar } = require('./secondMostPopular');

test('secondMostPopularChar', () => {
	const actual = secondMostPopularChar('');
	const expected = '';

	expect(expected).toBe(actual);
});

test('secondMostPopularChar', () => {
	const actual = secondMostPopularChar('abccdceffgihhhhj');
	const expected = 'c';

	expect(expected).toBe(actual);
});

test('secondMostPopularChar', () => {
	const actual = secondMostPopularChar('aabbbccc');
	const expected = 'a';

	expect(expected).toBe(actual);
});

test('secondMostPopularChar', () => {
	const actual = secondMostPopularChar('aaab');
	const expected = 'b';

	expect(expected).toBe(actual);
});