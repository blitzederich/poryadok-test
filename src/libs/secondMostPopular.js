// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

const { charCounter } = require('./charCounter');

/**
 * Вычисляет второй по популярности символ, входящий в переданную строку
 * @param {string} s 
 */
const secondMostPopularChar = (s) => {
	const chars = charCounter(s);

	const prepared 
	= Object
		.keys(chars)
		.map(c => ({ char: c, count: chars[c] }))
		.sort((a, b) => b.count - a.count)
		.filter((o, id, arr) => id > 0 ? !(o.count === arr[id - 1].count) : true);

	return prepared[1]?.char || '';
};

module.exports = {secondMostPopularChar};