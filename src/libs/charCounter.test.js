// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

const { charCounter } = require('./charCounter');

test('charCounter', () => {
	const actual = charCounter('eeeeeaaabbcccccc');
	const expected = {'e': 5, 'a': 3, 'b': 2, 'c': 6};

	expect(expected).toEqual(actual);
});