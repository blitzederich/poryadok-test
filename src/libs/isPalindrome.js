// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

/**
 * Определяет является ли строка палиндромом
 * @param {string} s проверяемая строка
 */
const isPalindrome = 
	s => s.toLowerCase() === s.toLowerCase().split('').reverse().join('');

module.exports = {isPalindrome};