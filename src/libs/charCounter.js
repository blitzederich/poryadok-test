// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

/**
 * Вычисляет количество вхождений каждого символа в строку
 * @param {string} s 
 * @return {{[key: string]: number}}
 */
const charCounter = (s) => {
	const counter = {};

	s.split('')
		.forEach(c => {
			if (counter[c] === undefined) {
				counter[c] = 0;
			}
			counter[c]++;
		});

	return counter;
};

module.exports = {charCounter};