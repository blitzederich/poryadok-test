// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

const { isPalindrome } = require('./isPalindrome');

test('isPalindrome', () => {
	const actual = isPalindrome('abAcabadabacaba');
	const expected = true;

	expect(expected).toBe(actual);
});