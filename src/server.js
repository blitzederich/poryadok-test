// Copyright 2022 Alexander Samorodov <blitzerich@gmail.com>

const express = require('express');
const { isPalindrome } = require('./libs/isPalindrome');
const { secondMostPopularChar } = require('./libs/secondMostPopular');

const PORT = process.env.PORT || 3000;

const app = express();

app.get('/api/secondMostPopularChar', (req, res) => {
	const arg = req.query.arg || '';
	res.json({ result: secondMostPopularChar(arg) });
});

app.get('/api/isPalindrome', (req, res) => {
	const arg = req.query.arg || '';
	res.json({ result: isPalindrome(arg) });
});

app.listen(PORT, () => console.log(`server started on ${PORT}`));